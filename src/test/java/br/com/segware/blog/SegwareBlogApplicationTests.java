package br.com.segware.blog;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.segware.blog.post.Post;
import br.com.segware.blog.post.PostService;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class SegwareBlogApplicationTests {
	
	@Autowired
	private PostService service;

	@Test
	public void contextLoads() {
	}
	
	@Test	
	public void testServiceListAll() {
		List<Post> posts = service.listAll();
		
		assertEquals(posts.size(), 3);
	}
	
	@Test
	public void testServiceFind() {
		Post post = service.find(1L);
		
		assertEquals(post.getEmail(), "barboza.oliveira@gmail.com");		
	}
	
	@Test	
	public void testServiceDelete() {
		Post post = new Post();
		post.setId(1L);
		service.delete(post);
		
		List<Post> posts = service.listAll();
		
		assertEquals(posts.size(), 2);
	}	
	
	@Test
	public void testCreateNewPost() {
		Post post = service.save(new Post("testNewPost@test.com", "Teste New Title", "Testing creating a new Post"));
		
		assertEquals(post.getId(), new Long(4L));		
	}
	
	@Test
	public void testUpdatePost() {
		Post post = service.find(1L);		

		post.setTitle("Title Change");
		post.setText("New Text test update");
		
		post = service.save(post);
		
		post = service.find(1L);	
		
		assertEquals(post.getEmail(), "barboza.oliveira@gmail.com");	
		assertEquals(post.getTitle(), "Title Change");
		assertEquals(post.getText(), "New Text test update");		
	}

}
