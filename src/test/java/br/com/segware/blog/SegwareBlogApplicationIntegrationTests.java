package br.com.segware.blog;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.segware.blog.post.Post;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class SegwareBlogApplicationIntegrationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void testListAllPosts() {

		ResponseEntity<List> responseEntity = restTemplate.getForEntity("/posts", List.class);
		List<Post> posts = responseEntity.getBody();

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(posts.size(), 3);
	}

	@Test
	public void testFindOnePost() {

		ResponseEntity<Post> responseEntity = restTemplate.getForEntity("/posts/1", Post.class);
		Post post = responseEntity.getBody();

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(post.getEmail(), "barboza.oliveira@gmail.com");
	}

	@Test
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void testDeleteOnePost() {

		restTemplate.delete("/posts/1");

		ResponseEntity<List> responseEntity = restTemplate.getForEntity("/posts", List.class);
		List<Post> posts = responseEntity.getBody();

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(posts.size(), 2);
	}

	@Test
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void testCreateOnePost() {

		ResponseEntity<Post> responseEntity = restTemplate.postForEntity("/posts",
				new Post("integrated@test.com", "Integration Test", "This is a integration test"), Post.class);
		Post post = responseEntity.getBody();

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(post.getEmail(), "integrated@test.com");

		ResponseEntity<List> responseEntityList = restTemplate.getForEntity("/posts", List.class);
		List<Post> posts = responseEntityList.getBody();

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(posts.size(), 4);
	}

	@Test
	public void testUpdateOnePost() {

		ResponseEntity<Post> responseEntity = restTemplate.getForEntity("/posts/1", Post.class);
		Post post = responseEntity.getBody();

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(post.getEmail(), "barboza.oliveira@gmail.com");

		post.setTitle("Title Change");
		restTemplate.put("/posts/1", post, Post.class);

		responseEntity = restTemplate.getForEntity("/posts/1", Post.class);
		post = responseEntity.getBody();

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(post.getTitle(), "Title Change");
	}

}
