package br.com.segware.blog;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.segware.blog.post.Post;
import br.com.segware.blog.post.PostRepository;

@SpringBootApplication
public class SegwareBlogApplication implements CommandLineRunner {
	
	@Autowired
	private PostRepository repository;	

	public static void main(String[] args) {
		SpringApplication.run(SegwareBlogApplication.class, args);
	}
	
	/**
	 * Method executed everytime that the applications run at first time.
	 */
	@Override
	public void run(String... args) throws Exception {
		
		Post p1 = new Post("barboza.oliveira@gmail.com", "Primeiro Post", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin hendrerit tellus vel neque venenatis, in elementum ante fringilla. Maecenas vel diam tempus nibh lacinia consectetur. Quisque sit amet ligula commodo, convallis metus quis, vestibulum sem. Pellentesque vel massa orci. Donec nec dictum dolor. Praesent at vehicula diam");
		Post p2 = new Post("teste@teste.com", "Segundo Post", "Nullam tincidunt faucibus ipsum. In interdum et tortor nec mollis. Nulla sit amet suscipit augue. Sed eu consectetur tortor. Nunc consectetur sit amet lectus non sagittis. Vestibulum dignissim nisi sed felis venenatis imperdiet. Nulla in nisl vel metus egestas pulvinar. Cras vehicula diam eu turpis");
		Post p3 = new Post("teste2@teste2.com", "Terceiro Post", "Aenean aliquet aliquet lectus. Vestibulum magna augue, sagittis at neque et, facilisis accumsan purus. Fusce sit amet rutrum elit, at blandit sapien. Aliquam consequat quam nec commodo gravida.");
		
		repository.saveAll(Arrays.asList(p1, p2, p3));
	}	
}
