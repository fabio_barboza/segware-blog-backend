package br.com.segware.blog.post;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * The entity that represents a Blog Post. 
 * 
 * @author fabio
 *
 */
@Entity
public class Post implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The Blog Post ID.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty(access = Access.READ_ONLY)	
	private Long id;

	/**
	 * The blog post creation date.
	 */
	@CreationTimestamp
	@JsonProperty(access = Access.READ_ONLY)	
	private Date date;

	/**
	 * The creator e-mail.
	 */
	@NotBlank
	@Size(min = 1, max = 254)	
	private String email;
	
	/**
	 * The Blog post title.
	 */
	@NotBlank
	@Size(min = 1, max = 150)		
	private String title;

	/**
	 * The Blog text content.
	 */
	@Lob
	@NotBlank
	@Size(min = 1)	
	private String text; 

	@JsonProperty(access = Access.READ_ONLY)
	private Long votes = 0L;

	public Post() {
		super();
	}

	public Post(String email, String title, String text) {
		this.email = email;
		this.title = title;
		this.text = text;
		this.date = new Date();
		this.votes = 0L;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getVotes() {
		return votes;
	}

	public void setVotes(Long votes) {
		this.votes = votes;
	}

	@Override
	public String toString() {
		return title;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Post other = (Post) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
