package br.com.segware.blog.post;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.segware.blog.exception.ObjectNotFoundException;

/**
 * The Blog post service.
 * 
 * @author fabio
 *
 */
@Service
public class PostService {

	/**
	 * The entity manager
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * The repository injection.
	 */
	@Autowired
	private PostRepository repository;

	/**
	 * List all Blog posts
	 * 
	 * @return A List with all Blog Posts order by creation date.
	 */
	public List<Post> listAll() {
		List<Post> result = new ArrayList<Post>();
		repository.findAll().forEach(result::add);
		return result.stream().sorted((p1, p2) -> p2.getDate().compareTo(p1.getDate())).collect(Collectors.toList());
	}
	
	/**
	 * Find a blog post by id.
	 * 
	 * @param id The post id.
	 * @return The Blod entity.
	 */
	public Post find(Long id) {
		return repository.findById(id).orElseThrow(() -> new ObjectNotFoundException("Registro não encontrado"));
	}
	
	/**
	 * Saves the blog post.
	 * 
	 * @param post The blog post entity.
	 * @return
	 */
	public Post save(Post post) {
		return repository.save(post);
	}
	
	/**
	 * Delete a Blog post.
	 * 
	 * @param post The blog post entity.
	 */
	public void delete(Post post) {
		repository.delete(post);
	}

}
