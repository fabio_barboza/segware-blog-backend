package br.com.segware.blog.post;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

/**
 * The resource Class responsible to listen the blog posts requisitions.
 * 
 * @author fabio
 *
 */
@RestController
@RequestMapping(value = "/posts")
public class PostResource {

	/**
	 * The service injection.
	 */
	@Autowired
	private PostService service;

	/**
	 * Find a blog post by id.
	 * 
	 * @param id
	 *            The blog post id.
	 * @return The blog post. @see Post.class
	 */
	@ApiOperation(value = "Return a blog Post entity.", response = Post.class)
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Long id) {
		return ResponseEntity.ok().body(service.find(id));
	}

	/**
	 * List all blog posts descending order of date creation.
	 * 
	 * @return A List of blog posts. @see Post.class
	 */
	@ApiOperation(value = "Return a list of blog posts.", response = Post.class)
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> list() {
		return ResponseEntity.ok().body(service.listAll());
	}

	/**
	 * Insert a new blog entry.
	 * 
	 * @param post
	 *            The post to inserted.
	 * 
	 * @return The persisted blog post.
	 */
	@ApiOperation(value = "Insert a new blog post entry.", response = Post.class)
	@PostMapping
	public ResponseEntity<?> insert(@Valid @RequestBody Post post) {
		return ResponseEntity.ok().body(service.save(post));
	}

	/**
	 * Update a blog post.
	 * 
	 * @param id
	 *            The post id.
	 * @param post
	 *            The edited post.
	 * @return The persisted blog post.
	 */
	@ApiOperation(value = "Updates a new blog post entry.", response = Post.class)
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@PathVariable Long id, @Valid @RequestBody Post post) {
		Post editedPost = service.find(id);

		editedPost.setDate(new Date());
		editedPost.setTitle(post.getTitle());
		editedPost.setText(post.getText());

		return ResponseEntity.ok().body(service.save(editedPost));
	}

	/**
	 * Delete a blog post.
	 * 
	 * @param id
	 *            The post id to be deleted.
	 */
	@ApiOperation(value = "Deletes a new blog post entry.")
	@DeleteMapping(value = "/{id}")
	public void delete(@PathVariable Long id) {
		Post post = new Post();
		post.setId(id);
		service.delete(post);
	}

	/**
	 * Votes in a blog post.
	 * 
	 * @param id
	 *            The blog post to be voted.
	 * @return The persisted blog post.
	 */
	@PutMapping("vote/{id}")
	@ApiOperation(value = "Increses the vote quantity by 1.", response = Post.class)
	public ResponseEntity<?> vote(@PathVariable Long id) {
		Post editedPost = service.find(id);

		editedPost.setVotes(editedPost.getVotes() + 1L);

		return ResponseEntity.ok().body(service.save(editedPost));
	}

}
