package br.com.segware.blog.post;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * The Post repository.
 * 
 * @author fabio
 *
 */
@Repository
public interface PostRepository extends CrudRepository<Post, Long>  {

}
