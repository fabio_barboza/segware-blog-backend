# segware-blog-backend

Backend do projeto de testes da empresa Segware.
O banco de dados adotado foi o H2 com gravação em disco para facilitar a execução da aplicação.
 
Está contido no projeto:

Build padrão do projeto via Maven.
Build da imagem docker.
Testes unitários e integrados.
Documentação da API via Swagger

Tecnologia: Spring Boot

## Live Sample

API pode ser acessada em uma instâcia do AWS EC2 através da URL:

http://ec2-13-59-22-163.us-east-2.compute.amazonaws.com:8080/

Documentação Swagger:

http://ec2-13-59-22-163.us-east-2.compute.amazonaws.com:8080/swagger-ui.html

## Rodando a aplicação

Para rodar a aplicação é necessário ter o Java 8 ou superior instalado.

Para rodar a aplicação em ambiente de testes execute o comando:

Linux

./mvnw spring-boot:run

Windows

mvnw.cmd spring-boot:run

## Gerando a imagem e rodando dentro do Docker

sudo ./mvnw install dockerfile:build

sudo docker run -p 8080:8080 -t springio/segware-blog

## Documentação Swagger Local

Foi criada uma documentação utilizando a API Swagger.
Após inicializar a aplicação a documentação via Swagger estará disponivel através da URL:

http://localhost:8080/swagger-ui.html

## Testando

Para executar o testes unitários utilize o comando:

Linux

./mvnw clean test

Windows

mvnw.cmd clean test

## Gerando o executável da aplicação

Linux

./mvnw clean package

Windows

mvnw.cmd clean package